# CKAD Tips

## Exam preparation

You can take the `Kubernetes Certified Application Developer (CKAD) with Tests` course of `Mumshad Mannambeth`. It's a complete course with many labs.

https://www.udemy.com/course/certified-kubernetes-application-developer/

To practice to the exam you can do these tasks :

https://github.com/dgkanatsios/CKAD-exercises

## Exam topics

Find the exam topics in this git repo (get the latest version) :

https://github.com/cncf/curriculum

## Exam rules

Make sure to read the documents provided by the cncf after registration to get the exam rules and the kubernetes version to use.

## Practice

You have to do much practice before passing the exam because this is key of success. You can install kubernetes locally using one node cluster like minikue.

https://kubernetes.io/docs/tasks/tools/install-minikube/

You can practice on one of the free online kubernetes platforms like :

https://www.katacoda.com/courses/kubernetes/playground

or

https://labs.play-with-k8s.com/

I prefer katacoda because the second platform is not stable.

But keep in mind that these platforms may not run the same kubernetes api version of your exam (generally an old version). So be careful. Otherwise, you can create your cluster on one the cloud platforms like Google or Amazon.

https://cloud.google.com/kubernetes-engine?hl=en

https://aws.amazon.com/fr/eks/

you'll have a free trial after making an account on one of these platforms.

## Skips questions

Skip low level questions (2% and 3%) as it takes much time to reach and understand the question.
You have access to a notebook which used to write down the questions you skipped.

As an alternative, you can use a file (to create in the file system) to store questions (something like skipped.txt).

## Aliases and Autocompletion

First thing when you get access to the terminal is to define aliases and autocompletion as you will save time using this tip :

    $ vim ~/.bashrc

and add this in the tail of the file :

```log
alias k=kubectl
alias kn='kubectl config set-context $(kubectl config current-context) --namespace '
source <(kubectl completion bash) # completion will save a lot of time and avoid typo
source <(kubectl completion bash | sed 's/kubectl/k/g' ) # so completion works with the alias "k"
```

To apply modifications, you just have to enter the command :

    $ source ~/.bashrc

or you can use the shorter version of the command:

    $ . ~/.bashrc

Now you can use k instead of kubectl and kn <namespace> to switch between namespaces. The autocompletion is enabled now you can use it in the command line.

## Background tasks

If a task, like deleting k8s objects, takes some time but you need to edit a yaml directly after, do this:

```log
$ kubectl -f delete stuff.yaml

# now press Ctrl+Z

$ vim stuff.yaml
```

To retrieve a background task you just type the command fg (for foreground).

## Flag questions

you can flag questions using the provided button so you can came back later and do it.

## Name yaml files

Name the yaml files starting with the question number for easy navigation throughout the exam. Example: something like naming `04-nginx-svc.yml` if question 4 was about exposing an nginx service. This also helps if you realize on a later question that you forgot a small detail, such as namespace missing from the YAML!

## Move to sudo

    $ sudo -i

## use command line documentation

You can at any time check the command line documentation. It's faster than going to the official documentation. To get the env specification for example, you can use this command line :

    $ kubectl explain pod.spec.containers.env --recursive

```log
KIND:     Pod
VERSION:  v1

RESOURCE: env <[]Object>

DESCRIPTION:
     List of environment variables to set in the container. Cannot be updated.

     EnvVar represents an environment variable present in a Container.

FIELDS:
   name	<string>
   value	<string>
   valueFrom	<Object>
      configMapKeyRef	<Object>
         key	<string>
         name	<string>
         optional	<boolean>
      fieldRef	<Object>
         apiVersion	<string>
         fieldPath	<string>
      resourceFieldRef	<Object>
         containerName	<string>
         divisor	<string>
         resource	<string>
      secretKeyRef	<Object>
         key	<string>
         name	<string>
         optional	<boolean>

```

## Using tmux and screen (optional)

You can use one of these programs to split the terminal for example to show the pod specification or for any other purpose. I didn't use this technique in the exam because i faced some issues of copy/paste when i was training for the exam on one of the online platforms. It's up to you to use it if you want.

Here is some useful tmux commands.

    $ tmux ls

list all sessions

    $ tmux

log into the first session : the one showed from the last command

To use tmux you have to prefix all commands with a leader (or prefix) which is Ctrl+B in our case.

### create session

Create a new session :

    $ tmux new -s session_name

Detaching from Tmux Session :

    $ Ctrl+B , d

Re-attaching to Tmux Session

    $ tmux attach-session -t 0

`0` is the session number, run `ls` to list all sessions.

```log
ckad: 1 windows (created Thu May  7 18:28:17 2020) [204x52]
```

and `ckad` in the last one.

### Navigation

    $ Ctrl+B , c

c for create. this will create a new window (plus the one created when logged into the session).

    $ Ctrl+B , n

n for next. To navigate between windows.

    $ Ctrl+B , c

To create a third window.

    $ Ctrl+B, p

To move back to the previous window.

    $ Ctrl+B , 7

To move to the 7th window

    $ Ctrl+c , Ctrl+d, Ctrl+B X

To quit the current window

    $ Ctrl+B , %

Split the window vertically

    $ Ctrl+B , "

Split the window horizontally

    $ Ctrl+B , Alt , -> or Ctrl+B , Alt , <-

resize the current window

    $ exit

To exit from one of the windows

    $ Ctrl+B , o

To move from a split window to another

    $ Ctrl+B , z

Show the current window in full screen

    $ Ctrl+B , z

To go back to the last configuration

    $ exit

To quit tmux

## Vi and Shell Shortcuts (optional)

Just a reminder.

### Shell

1. CTRL+A – Quickly move to the beginning of line.

2. CTRL+U – Delete all characters before the cursor (Kills backward from point to the beginning of line).

3. CTRL+L – Clears the screen and redisplay the line.

4. CTRL+T – Swaps the last two characters.

5. CTRL+R – Searches the history backward (Reverse search).

6. CTRL+W – Delete the word before the cursor. (don't use it because it'll will close the current browser tab also)

7. CTRL+E – Move to the end of line

8. CTRL+X – Lists the possible filename completions of the current word.

9. CTRL+XX – Move between start of command line and current cursor position (and back again).

10. CTRL+Y – Retrieves last item that you deleted or cut.

### Vi

- search :

/word-to-search : type n and N to move forward and backward

- delete line :

dd

- delete current word :

dw

- undo :

u

- redo :

Ctrl+R

- Go to the end of the line :

\$

- Go to the end of the line and switch to the editing mode :

A

- display line numbers

:set number

- hide line numbers

:set nonumber

- copy/cut and paste

Position the cursor where you want to begin cutting.

Press `v` to select characters, or uppercase `V` to select whole lines, or `Ctrl-v` to select rectangular blocks (use `Ctrl-q` if `Ctrl-v` is mapped to paste).

Move the cursor to the end of what you want to cut.

Press `d` to cut (or `y` to copy).

Move to where you would like to paste.

Press `P` to paste before the cursor, or `p` to paste after.

- delete a block of lines

In Vim, use visual line mode:

Put your cursor on the top line of the block of text/code to remove.

Press V (That's capital "V" : Shift + v )

Move your cursor down to the bottom of the block of text/code to remove.

Press d.
